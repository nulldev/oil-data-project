declare module google{
	module maps{
		
		class MapTypeId{
			static SATELLITE: string;
		}
		
		interface IMapOptions{
            streetViewControl: boolean;
            panControl: boolean;
            zoomControl: boolean;
            scaleControl: boolean;
            mapTypeId: string;
		}
		
		class Map{
			constructor(element: HTMLElement, options: IMapOptions);
			fitBounds(bounds: LatLngBounds);
		}
		
		class LatLngBounds{
			extend(latlng: LatLng): void;
		}
		
		class LatLng{
			constructor(lat: number, lng: number);
		}
		
		class Animation{
			static Bounce: string;
		}
		
		interface IMarkerOptions{
			position: LatLng,
			map: Map,
			animation: string;
			title: string;
		}
		
		class Marker{
			constructor(options: IMarkerOptions)
		}
	}
}