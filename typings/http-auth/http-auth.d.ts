declare module 'http-auth' {
	import * as express from 'express';
	
	class Base{
		
	}
	
	class Basic extends Base{
		constructor()
	}
	
	interface IBasicAuthParams{
		realm: string;
		file: string;
	}
	
	export function basic(params: IBasicAuthParams): Basic;
	export function connect(auth: Base): express.RequestHandler;
}