﻿var kafka = require('kafka-node'),
	Consumer = kafka.Consumer,
	client = new kafka.Client(),
	consumer = new Consumer(
		client,
		[{ topic: 'production-published', partition: 0 }],
		{
			groupId: 'oil-data-node-group',
			autoCommit: false
		}
	);

console.log('Listening for topic "oil-data"...');

consumer.on('message', function (message) {
	var value = JSON.parse(message.value);
	console.log('Received: ' + value.TimeStamp);
});


var terminate = function () {
	console.log('Terminating Kafka consumer...');
	consumer.close(function () {
		client.close(function () {
			process.exit();
		});
	});
}

process.on('SIGINT', terminate);
