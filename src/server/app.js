var express = require('express');
var http = require('http');
var socket = require('socket.io');
var auth = require('http-auth');
var kafka_node_1 = require('kafka-node');
var logger = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser');
var compress = require('compression');
var favicon = require('serve-favicon');
var port = process.env.PORT || 8085;
var environment = process.env.NODE_ENV;
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(compress()); // Compress response data with gzip
app.use(logger('dev'));
app.use(favicon(__dirname + '/favicon.ico'));
app.use(cors()); // enable ALL CORS requests
var server = http.Server(app);
var io = socket(server);
var basic = auth.basic({
    realm: 'Oil Data.',
    file: __dirname + '/../data/users.htpasswd'
});
var client = new kafka_node_1.Client(), consumer = new kafka_node_1.Consumer(client, [{ topic: 'production-published' }], {
    groupId: 'oil-data-node-group',
    autoCommit: false
});
app.use(auth.connect(basic));
console.log('About to crank up node');
console.log('PORT=' + port);
console.log('NODE_ENV=' + environment);
var source = '';
app.get('/ping', function (req, res, next) {
    console.log(req.body);
    res.send('pong');
});
console.log('** DEV **');
console.log('serving from ' + './src/client/ and ./');
app.use('/', express.static('./src/client/'));
app.use('/', express.static('./'));
console.log('waiting for socket connection');
// Real time with socket.io
io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('well selected', function (selectedWell) {
        consumer.on('message', function (message) {
            var data = JSON.parse(message.value);
            console.log('Received from Kafka: ' + data.TimeStamp);
            sendProductionData(data);
        });
    });
    socket.on('disconnect', function () {
        console.log('a user disconnected');
    });
});
server.listen(port, function () {
    console.log('Express server listening on port ' + port);
    console.log('env = ' + app.get('env') +
        '\n__dirname = ' + __dirname +
        '\nprocess.cwd = ' + process.cwd());
});
var sendProductionData = function (data) {
    var d = new Date();
    io.emit('well data', data);
    process.stdout.write('Well data sent: ' + data.TimeStamp + '\033[0G');
};
