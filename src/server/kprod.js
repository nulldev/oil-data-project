﻿var kafka = require('kafka-node'),
	Producer = kafka.Producer,
	KeyedMessage = kafka.KeyedMessage,
	client = new kafka.Client(),
	producer = new Producer(client),
	timer = null;

producer.on('ready', function () {

	console.log('Producer ready. Sending data...');

	timer = setInterval(function () {
		var data = getProductionData();
		producer.send([{
			topic: 'production-published',
			messages: JSON.stringify(data)
		}], function (err) {
			if (err) {
				console.log('Error sending data: ' + err);
				terminate();
			} else {
				console.log('Data sent: ' + data.TimeStamp);
			}
		});

	}, 1000);

});

producer.on('error', function (err) {
	console.log('Error' + err);
	terminate();
});


var random = function (min, max) {
	return (Math.random() * (max - min)) + min;
};

var getProductionData = function () {
	var d = new Date();

	return addData({
		"WellId": 441023,
		"TimeStamp": d.getTime(),
		"Status": "Producing"
	});
};

var terminate = function () {
	console.log('Terminating Kafka producer...');
	clearInterval(timer);
	client.close(function () {
		process.exit();
	});
}

process.on('SIGINT', terminate);


function DataGen(name, min, max) {
	var lastValue = null;

	this.getName = function () {
		return name;
	}

	this.getNextValue = function () {
		if (lastValue === null) {
			lastValue = random(min, max)
		} else {
			var direction = Math.random() > 0.5 ? 1 : -1;
			var val = 0.1 * (max - min) * Math.random() * direction;
			lastValue = Math.max(min, Math.min(lastValue + val, max));
		}
		
		
		return lastValue;
	}

};


var dataSet = [
	new DataGen('Oil_Volume_Produced', 0.2, 0.7),
	new DataGen('Gas_Volume_Produced', 0.8, 1.2),
	new DataGen('Water_Volume_Produced', 0.03, 0.08),
	new DataGen('THP', 4000, 7000),
	new DataGen('CHP', 400, 600),
	new DataGen('BHP', 400, 600),
	new DataGen('SIBHP', 3000, 5000),
	new DataGen('Surface_Temperature', 20, 25),
	new DataGen('BH_Temperature', 350, 450),

	new DataGen('ROP Avg', 0, 500),
	new DataGen('WOB Avg', 0, 50),
	new DataGen('Hookload', 50, 250),
	new DataGen('Block Position', 0, 120),
	new DataGen('SPP Avg', 0, 200),
	new DataGen('Flow In', 0, 100),
	new DataGen('Torque Abs Avg', 0, 10),
	new DataGen('RPM Surface Avg', 0, 200)
];

var addData = function (o) {
	dataSet.forEach(function (ds) {
		o[ds.getName()] = ds.getNextValue();
	});

	return o;
}
