import * as express from 'express';
import * as http from 'http';
import * as socket from 'socket.io';
import * as auth from 'http-auth';
import { Consumer,  Client} from 'kafka-node';
import * as logger from 'morgan';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as compress from 'compression';
import * as favicon from 'serve-favicon';

var port: number = process.env.PORT || 8085;
var environment: string = process.env.NODE_ENV;

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(compress()); // Compress response data with gzip
app.use(logger('dev'));
app.use(favicon(__dirname + '/favicon.ico'));
app.use(cors()); // enable ALL CORS requests

var server = http.Server(app);

var io = socket(server);

var basic = auth.basic({
	realm: 'Oil Data.',
	file: __dirname + '/../data/users.htpasswd'
});

var client = new Client(),
	consumer = new Consumer(
		client,
		[{ topic: 'production-published' }],
		{
			groupId: 'oil-data-node-group',
			autoCommit: false
		}
	);

app.use(auth.connect(basic));

console.log('About to crank up node');
console.log('PORT=' + port);
console.log('NODE_ENV=' + environment);

var source = '';

app.get('/ping', (req, res, next) => {
	console.log(req.body);
	res.send('pong');
});

console.log('** DEV **');
console.log('serving from ' + './src/client/ and ./');
app.use('/', express.static('./src/client/'));
app.use('/', express.static('./'));

console.log('waiting for socket connection');

// Real time with socket.io
io.on('connection', socket => {
	console.log('a user connected');
	socket.on('well selected', selectedWell => {
		consumer.on('message', message => {
			var data = JSON.parse(message.value);
			console.log('Received from Kafka: ' + data.TimeStamp);
			sendProductionData(data);
		});
	});

	socket.on('disconnect', () => {
		console.log('a user disconnected');
	});
});

server.listen(port, () => {
	console.log('Express server listening on port ' + port);
	console.log('env = ' + app.get('env') +
		'\n__dirname = ' + __dirname +
		'\nprocess.cwd = ' + process.cwd());
});

var sendProductionData = data => {
	var d = new Date();
	io.emit('well data', data);
	process.stdout.write('Well data sent: ' + data.TimeStamp + '\033[0G');
}
