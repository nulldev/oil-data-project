(() : void => {
    'use strict';
    
    angular.module('app', ['ui.bootstrap', 'uiGmapgoogle-maps']);    
})();
