((): void => {
    'use strict';
    angular
        .module('app')
        .directive('ongFlotPlot', ongFlotPlot);

    function ongFlotPlot(): ng.IDirective {

        var directive = <ng.IDirective>{
            link: link,
            templateUrl: 'app/components/ong-flot-plot.directive.html',
            replace: true,
            restrict: 'E',
            scope: {
                title: "@",
                data: "="
            }
        }

        function link(scope, elem, attrs) {
            if (!attrs.curves) {
                return;
            }
            var plotData = [];
            var curves = attrs.curves.split('|');
            _.forEach(curves, cname => {
                plotData.push({
                    label: cname,
                    data: []
                });
            });
            var container = $(elem).find("#flot-line-chart");
            var plot = $.plot(container, [], {
                yaxis: {
                    mode: 'time',
                    timezone: 'browser',
                    autoscaleMargin: 0,
                    transform: v => -v,
                    inverseTransform: v => -v
                },
                xaxis: {
                    position: 'top'
                }
            });
            scope.$watch(attrs.data, rtData => {
                if (!rtData) {
                    return;
                }
                scope.lastUpdated = (new Date(rtData.TimeStamp)).toLocaleTimeString();
                plot.setData(getPlotData(plotData, rtData, +attrs.points));
                plot.setupGrid();
                plot.draw();
            });
        }

        return directive;
    }

    var getPlotData = (plotData, rtData, maxPoints) => {
        _.forEach(plotData, (pd : any) => {
            if (rtData[pd.label] !== undefined) {
                pd.data.push([rtData[pd.label], rtData.TimeStamp]);
                if (pd.data.length > maxPoints) {
                    pd.data = _.drop(pd.data); // remove the first point
                }
            }
        });
        return plotData;
    };
})();
