((): void => {
    'use strict';
    angular
        .module('app')
        .directive('ongFlotChart', ongFlotChart);

    function ongFlotChart(): ng.IDirective {

        var directive = <ng.IDirective>{
            link: link,
            templateUrl: 'app/components/ong-flot-chart.directive.html',
            replace: true,
            restrict: 'E',
            scope: {
                title: "@",
                data: "="
            }
        }

        function link(scope: ng.IScope, elem: ng.IAugmentedJQuery, attrs) {
            if (!attrs.curves) {
                return;
            }
            var plotData = [];
            var curves = attrs.curves.split('|');
            _.forEach(curves, cname => {
                plotData.push({
                    label: cname,
                    data: []
                });
            });
            var container = $(elem).find("#flot-line-chart");
            var plot = $.plot(container, [], {
                xaxis: {
                    mode: 'time',
                    timezone: 'browser'
                }
            });


            scope.$watch(attrs.data, rtData => {
                if (!rtData) {
                    return;
                }
                scope.lastUpdated = (new Date(rtData.TimeStamp)).toLocaleTimeString();
                plot.setData(getPlotData(plotData, rtData, +attrs.points));
                plot.setupGrid();
                plot.draw();
            });
        }

        return directive;
    }

    var getPlotData = function(plotData, rtData, maxPoints) {
        _.forEach(plotData, function(pd) {
            if (rtData[pd.label] !== undefined) {
                pd.data.push([rtData.TimeStamp, rtData[pd.label]]);
                if (pd.data.length > maxPoints) {
                    pd.data = _.drop(pd.data); // remove the first point
                }
            }
        });
        return plotData;
    };
})();
