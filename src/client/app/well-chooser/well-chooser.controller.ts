module app.wellchooser {
    'use strict';

    interface WellChooserControllerScope {

    }

    class WellChooserController {
        isBusy: boolean = true;
        regions: services.IWellHeirarchy[];
        selectedWell: string;

        static $inject = ['$modalInstance', 'app.services.WellDataService', 'app.services.WellMapService'];
        constructor(private $modalInstance, private wellDataService: services.IWellDataService, private wellMapService: services.IWellMapService) {
            $modalInstance.rendered.then(() => this.getData());
        }

        showWellsOnMap() {
            var wells: services.IWellHeirarchy[] = [];
            this.regions.forEach(region => {
                if (region.nodes) {
                    region.nodes.forEach(area => {
                        if (area.nodes) {
                            area.nodes.forEach(well => {
                                wells.push(well);
                            });
                        }
                    });
                }
            });

            this.wellMapService.mapWells(wells);
        }

        private getData() {
            this.wellDataService.getMasterData()
                .then(data => {
                    this.isBusy = false;
                    this.regions = data;
                    this.showWellsOnMap();
                });
        }

        selectWell(well: string) {
            this.selectedWell = well;
        }

        ok() {
            this.$modalInstance.close(this.selectedWell);
        }

        cancel() {
            this.$modalInstance.dismiss('cancel');
        };
    }

    angular.module('app')
        .controller('app.wellchooser.WellChooserController', WellChooserController);
}
