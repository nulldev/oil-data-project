module app.controllers {
    'use strict';

    interface IMainControllerScope {
        alerts: services.IAlarm[];
        selectedWell: string;
        openWellChooser(): void;
        manageAlarms(): void;
        removeAlert(alert: services.IAlarm): void;
    }

    class MainController implements IMainControllerScope {
        alerts: services.IAlarm[] = [];
        selectedWell: string;

        static $inject = ['$scope', '$modal', '$interval', 'app.services.AlarmService'];
        constructor(private $scope: ng.IScope, private $modal, private $interval: ng.IIntervalService, private alarmService: services.IAlarmService) {
        }

        getMarker(lat: number, long: number): google.maps.LatLng {
            return new google.maps.LatLng(lat, long);
        }

        openWellChooser(): void {
            var modalInstance = this.$modal.open({
                animation: true,
                templateUrl: 'app/well-chooser/well-chooser.template.html',
                controller: 'app.wellchooser.WellChooserController as vm',
                size: 'lg'
            });

            modalInstance.result.then(selectedWell => {
                this.selectedWell = selectedWell;
                var socket = io();
                socket.emit('well selected', selectedWell.text);
                socket.on('well data', data => {
                    this.$scope.$apply(() => {
                        this.data = data;
                        this.alarmService.setNameList(data);
                        _.forEach(this.alarmService.getAlarms(data), alarm => {
                            this.alerts.push(alarm);
                        });
                    });
                });
            });
        }

        manageAlarms(): void {
            var modalInstance = this.$modal.open({
                animation: true,
                templateUrl: 'app/alarm-manager/alarm-manager.template.html',
                controller: 'app.alarmmanager.AlarmManagerController as vm',
                size: 'lg'
            });

            modalInstance.result.then(function() { });
        }

        removeAlert(alert: services.IAlarm): void {
            _.remove(this.alerts, alert);
        }
    }

    angular.module('app')
        .controller('app.controllers.MainController', MainController);
}
