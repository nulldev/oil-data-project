﻿module app.alarmmanager {
    'use strict';

    interface IAlarmManagerScope {
        alarmConditions: app.services.IAlarmCondition[];
        availableNames: string[];
        availableOperations: string[];
                 
        addNewAlarmCondition(): void;
        removeCondition(ac): void;
        ok(): void;
        cancel(): void;
    }

    class AlarmManagerController implements IAlarmManagerScope {
        alarmConditions: app.services.IAlarmCondition[];
        availableNames: string[];
        availableOperations: string[];

        static $inject = ['$modalInstance', 'app.services.AlarmService'];        
        constructor(private $modalInstance, private alarmService: app.services.IAlarmService) {
            var vm = this;

            vm.alarmConditions = alarmService.getAlarmConditions();
            vm.availableNames = alarmService.getNameList();
            vm.availableOperations = alarmService.getAvailableOperations();
        }

        addNewAlarmCondition() {
            this.alarmConditions.push(this.alarmService.getNewAlarm());
        };

        removeCondition(ac) {
            _.remove(this.alarmConditions, ac);
        }

        ok() {
            this.alarmService.setAlarmConditions(this.alarmConditions);
            this.$modalInstance.close();
        };

        cancel() {
            this.$modalInstance.dismiss('cancel');
        };
    }

    angular.module('app')
        .controller('app.alarmmanager.AlarmManagerController', AlarmManagerController);
}
