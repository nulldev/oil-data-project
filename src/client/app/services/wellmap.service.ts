module app.services {
    'use strict';

    export interface IWellMapService {
        mapWells(wells: IWellHeirarchy[]): void;
    }

    class WellMapService implements IWellMapService {

        private mapOptions = {
            streetViewControl: false,
            panControl: false,
            zoomControl: false,
            scaleControl: false,
            mapTypeId: google.maps.MapTypeId.SATELLITE
        };

        private map : google.maps.Map;
        constructor() {

        }

        mapWells(wells: IWellHeirarchy[]): void {

            if (!this.map) {
                this.map = new google.maps.Map($('#map-canvas')[0], this.mapOptions);
            }

            var bounds = new google.maps.LatLngBounds();

            wells.forEach(well => {
                var latLng = new google.maps.LatLng(well.Lat, well.Long);
                new google.maps.Marker({
                    position: latLng,
                    map: this.map,
                    animation: google.maps.Animation.Bounce,
                    title: well.text
                });

                bounds.extend(latLng);
            });

            this.map.fitBounds(bounds);
        }
    }

    angular.module('app')
        .service('app.services.WellMapService', WellMapService);
}
