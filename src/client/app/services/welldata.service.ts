module app.services {
    export interface IWellDataService {
        getMasterData(): ng.IPromise<IWellHeirarchy[]>;
        getSecondData(wellId: string): ng.IPromise<ISecondData>;
    }

    export interface IWellHeirarchy {
        nodes: IWellHeirarchy[];
        Lat: number;
        Long: number;
        text: string;
    }

    export interface ISecondData {

    }

    class WellDataService implements IWellDataService {

        static $inject = ['$http'];
        constructor(private $http: ng.IHttpService) {

        }

        getMasterData(): ng.IPromise<IWellHeirarchy[]> {
            return this.$http.get("/master")
                .then(function(response) {
                    return response.data[0].hierarchy;
                });
        }

        getSecondData(wellId: string): ng.IPromise<ISecondData> {
            return this.$http.get('/seconddata?wellid=' + wellId)
                .then(function(response) {
                    return response.data;
                });
        }
    }

    angular.module('app')
        .service('app.services.WellDataService', WellDataService);
}
