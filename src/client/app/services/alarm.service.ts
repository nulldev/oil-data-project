﻿module app.services {
    'use strict';

    export interface IAlarmService {
        getAlarms(data: any): IAlarm[];
        getAlarmConditions(): IAlarmCondition[];
        setAlarmConditions(conditions: IAlarmCondition[]): void;
        setNameList(names: string[]): void;
        getNameList(): string[];
        getAvailableOperations(): string[];
        getNewAlarm(): IAlarmCondition;
    }

    export interface IAlarm {
        text: string;
        value: string;
    }

    export interface IAlarmCondition {
        name: string;
        op: string;
        limit: number;
    }

    class AlarmService implements IAlarmService {
        private alarmConditions: IAlarmCondition[] = [];
        private nameList: string[] = [];

        getAlarms(data): IAlarm[] {
            var alarms = [];
            _.forEach(this.alarmConditions, function(ac) {
                var value = +data[ac.name];

                if (value !== void 0) {
                    if (ac.op === '>' && value > ac.limit) {
                        alarms.push({
                            text: 'High ' + ac.name,
                            value: value.toFixed(2)
                        });
                    } else if (ac.op === '<' && value < ac.limit) {
                        alarms.push({
                            text: 'Low ' + ac.name,
                            value: value.toFixed(2)
                        });
                    }
                }
            });

            return alarms;
        }

        getAlarmConditions(): IAlarmCondition[] {
            return _.defaultsDeep<IAlarmCondition[], IAlarmCondition[]>([], this.alarmConditions);
        }

        setAlarmConditions(conditions: IAlarmCondition[]) {
            this.alarmConditions = conditions;
        }

        getNameList(): string[] {
            return this.nameList;
        }

        setNameList(names: string[]) {
            if (this.nameList.length > 0) {
                return;
            }

            _.forEach(names, function(val, key) {
                if (typeof val === 'number') {
                    this.nameList.push(key);
                }
            });
        }

        getAvailableOperations(): string[] {
            return ['<', '>'];
        }

        getNewAlarm() {
            return {
                name: this.nameList[0],
                op: '<',
                limit: 0
            }
        }
    }

    angular.module('app')
        .service('app.services.AlarmService', AlarmService);
}